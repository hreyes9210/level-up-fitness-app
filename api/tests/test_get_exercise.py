from fastapi.testclient import TestClient
from main import app
from models.exercise_models import ExerciseOut
from queries.exercises import ExerciseRepository
from authenticator import authenticator


client = TestClient(app)


class MockExerciseRepository:
    """
    a mock version of exercise repository
    """

    def get_one(self, user_id: int, exercise_id: int):
        #  return a list of mock exercise objects
        return ExerciseOut(
            id=exercise_id,
            user_id=user_id,
            name="string",
            type="string",
            muscle="string",
            equipment="string",
            difficulty="string",
            instructions="string",
            sets=0,
            reps=0,
            time=0,
            exercise_date="2024-02-07",
        )


def mock_get_current_account_data():
    """
    mock current account data
    """
    return {
        "id": 1,
        "username": "test1",
        "email": "test1@example.com",
        "hashed_password":
        "$2b$12$Q8wagccMWsV0TkhcVTPt0u/Rq8oVT5H4HVycJRIruyrnMzwG4A7Um",
    }


def test_get_exercise():
    """
    test the get exercises endpoint
    """
    # Arrange
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_get_current_account_data
    app.dependency_overrides[ExerciseRepository] = MockExerciseRepository

    exercise_id = 1

    # Act
    response = client.get(f"/api/exercises/{exercise_id}")

    # Clean up
    app.dependency_overrides = {}

    expected_response = {
        "id": 1,
        "user_id": 1,
        "name": "string",
        "type": "string",
        "muscle": "string",
        "equipment": "string",
        "difficulty": "string",
        "instructions": "string",
        "sets": 0,
        "reps": 0,
        "time": 0,
        "exercise_date": "2024-02-07",
    }

    # Assert
    assert response.status_code == 200
    assert response.json() == expected_response
