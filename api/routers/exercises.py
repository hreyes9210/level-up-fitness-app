from fastapi import APIRouter, Depends, HTTPException, status
from queries.exercises import ExerciseIn, ExerciseRepository, ExerciseOut
from models.thirdp_exercise_models import thirdPartyExerciseItem
from exercises_thirdp_request import thirdPartyExerciseRepository
from typing import List
from datetime import date
from authenticator import authenticator

router = APIRouter()


@router.post("/api/exercises", response_model=ExerciseOut)
def create_exercise(
    exercise: ExerciseIn,
    repo: ExerciseRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    try:
        new_exercise = repo.create(user_id=account["id"], exercise=exercise)
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create exercise",
        )
    return new_exercise


@router.get("/api/exercises", response_model=List[ExerciseOut])
def get_all(
    repo: ExerciseRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    try:
        user_exercises = repo.get_all(user_id=account["id"])
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot get exercise",
        )
    return user_exercises


@router.put("/api/exercises/{exercise_id}", response_model=ExerciseOut)
def update_exercise(
    exercise_id: int,
    exercise: ExerciseIn,
    repo: ExerciseRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> ExerciseOut | Exception:
    try:
        updated_exercise = repo.update_exercise(
            user_id=account["id"], exercise_id=exercise_id, exercise=exercise
        )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot update exercise",
        )
    return updated_exercise


@router.delete("/api/exercises/{exercise_id}", response_model=bool)
def delete_exercise(
    exercise_id: int,
    repo: ExerciseRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> bool:
    return repo.delete(user_id=account["id"], exercise_id=exercise_id)


@router.get("/api/exercises/{exercise_id}", response_model=ExerciseOut)
def get_one_exercise(
    exercise_id: int,
    repo: ExerciseRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> ExerciseOut:
    return repo.get_one(user_id=account["id"], exercise_id=exercise_id)


@router.get("/api/workouts/{exercise_date}", response_model=List[ExerciseOut])
def get_workout(
    exercise_date: date,
    repo: ExerciseRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> List[ExerciseOut]:
    return repo.get_workout(user_id=account["id"], exercise_date=exercise_date)


@router.get(
    "/api/thirdp_exercises/{muscle}",
    response_model=List[thirdPartyExerciseItem],
)
def get_exercise(
    muscle: str, repo: thirdPartyExerciseRepository = Depends()
) -> List[thirdPartyExerciseItem]:
    return repo.get_exercises(muscle)
