from pydantic import BaseModel
from typing import Optional
from datetime import date


class Error(BaseModel):
    message: str


class ExerciseIn(BaseModel):
    name: str
    type: str
    muscle: str
    equipment: str
    difficulty: str
    instructions: str
    sets: Optional[int]
    reps: Optional[int]
    time: Optional[int]
    exercise_date: date


class ExerciseOut(BaseModel):
    id: int
    user_id: int
    name: str
    type: str
    muscle: str
    equipment: str
    difficulty: str
    instructions: str
    sets: Optional[int]
    reps: Optional[int]
    time: Optional[int]
    exercise_date: date
