from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class AccountIn(BaseModel):
    username: str
    email: str
    password: str


class AccountOutWithHashedPassword(BaseModel):
    id: int
    username: str
    email: str
    hashed_password: str


class AccountOut(BaseModel):
    id: int
    username: str
    email: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class DuplicateAccountError(ValueError):
    pass
